const express = require("express");
const cors = require("cors");
const rooms = require("./rooms.js");
const bodyParser = require("body-parser");
const app = express();
var path = require('path');

app.use('/', express.static(path.join(__dirname, 'public')))

function getProduct(productId) {
  let product = null;

  rooms.forEach((p) => {
    if (p.sys.id == productId) {
      product = p;
    }
  });

  return product;
}

app.use(cors());
app.use(bodyParser.json());

app.listen(4000, function () {
  console.log("Listening on port 4000...");
});

app.get("/rooms", (req, res) => {
  res.json(rooms);
});

app.get("/rooms/:id", (req, res) => {
  let product = getProduct(req.params.id);

  if (product) {
    res.json(product);
  } else {
    res.sendStatus(404);
  }
});
