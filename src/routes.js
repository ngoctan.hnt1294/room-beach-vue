import Home from "./HomeComponent/Home";
import Rooms from "./RoomsComponent/Rooms.vue";
import ViewRoom from "./ViewRoom";

export const routes = [
  { path: "", name: "Home", component: Home },
  { path: "/rooms/", name: "Rooms", component: Rooms },
  {
    path: "/rooms/:roomsId",
    name: "ViewRoom",
    props: true,
    component: ViewRoom,
  },
  { path: "*", component: { template: "<h1>Page Not Found!</h1>" } },
];
